﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.IO;
using UnityEngine.UI;

public class TwitchChat : MonoBehaviour
{
    private TcpClient twitchClient;
    private StreamReader reader;
    private StreamWriter writer;

    public GameObject item, item2, item3, item4;
    public float randX;
    public Vector2 whereToSpawn;
    public float spawnRate = 2f;
    public float nextSpawn = 0.0f;

    public string username, password, channelName;

    public Text chatBox;
    public Rigidbody2D player;
    public int speed;


    void Start()
    {
        Connect();
    }

    // Update is called once per frame
    void Update()
    {
        if (!twitchClient.Connected)
        {
            Connect();

        }

        ReadChat();


    }


    private void Connect()
    {
        twitchClient = new TcpClient("irc.chat.twitch.tv", 6667);
        reader = new StreamReader(twitchClient.GetStream());
        writer = new StreamWriter(twitchClient.GetStream());

        writer.WriteLine("PASS " + password);
        writer.WriteLine("NICK " + username);
        writer.WriteLine("USER " + username + " 8 * :" + username);
        writer.WriteLine("JOIN #" + channelName);
        writer.Flush();

    }

    private void ReadChat()
    {
        if (twitchClient.Available > 0)
        {
            var message = reader.ReadLine();

            if (message.Contains("PRIVMSG"))
            {
                var splitPoint = message.IndexOf("!", 1);
                var chatName = message.Substring(0, splitPoint);
                chatName = chatName.Substring(1);

                splitPoint = message.IndexOf(":", 1);
                message = message.Substring(splitPoint + 1);
                print(String.Format("{0}: {1}", chatName, message));
                chatBox.text = chatBox.text + "\n" + String.Format("{0}: {1}", chatName, message);
                GameImputs(message);
            }


        }
    }

    private void GameImputs(string ChatInputs)
    {

        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randX = UnityEngine.Random.Range(-104f, -90f);
            whereToSpawn = new Vector2(randX, transform.position.y);

            switch (ChatInputs.ToLower())
            {
                case "serra":
                    Instantiate(item, whereToSpawn, Quaternion.identity);
                    break;
                case "inimigo":
                    Instantiate(item2, whereToSpawn, Quaternion.identity);
                    break;
                case "canhao":
                    Instantiate(item3, whereToSpawn, Quaternion.identity);
                    break;
                case "teste":
                    Instantiate(item4, whereToSpawn, Quaternion.identity);
                    break;
            }


        }

    }
}
